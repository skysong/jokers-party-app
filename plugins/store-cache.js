const cachePrefixKey = "jokers-party-app-";
const currentNavIdCache = cachePrefixKey + "currentNavId";
export default ({app}) => {
  const store = app.store;
  if (sessionStorage.getItem(currentNavIdCache)) {
    const temp = {
      currentNavId: sessionStorage.getItem(currentNavIdCache)
    };
    store.replaceState(Object.assign({}, store.state, temp))
  }
  // 在页面刷新时将vuex里的信息保存到sessionStorage里
  window.addEventListener('beforeunload', () => {
    sessionStorage.setItem(currentNavIdCache, store.state.currentNavId)
  });
  window.addEventListener('pagehide', () => {
    sessionStorage.setItem(currentNavIdCache, store.state.currentNavId)
  })
}
