export default {
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Jokers\' Party',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'},
      {
        hid: 'description',
        name: 'description',
        content: 'Jokers\' Party NFTs are 10,000 unique, digitally mastered avatars lifting aggressively on the Ethereum blockchain.'
      },
      {name: 'theme-color', content: '#000000'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'shortcut', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'apple-touch-icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/lib/bootstrap-5.2.2/css/bootstrap.min.css',
    '@/assets/lib/animate-4.1.1/animate.min.css',
    '@/assets/lib/material-icons-6.9.6/css/materialdesignicons.min.css',
    '@/assets/css/style.css',
    '@/assets/css/responsive.css',
    '@/assets/css/better-scroller.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~/plugins/store-cache.js', ssr: false}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    '@nuxtjs/axios',
    'nuxt-precompress',
  ],

  nuxtPrecompress: {
    gzip: {
      enabled: true,
      filename: '[path].gz[query]',
      threshold: 10240,
      minRatio: 0.8,
      compressionOptions: {level: 9},
    },
    brotli: {
      enabled: true,
      filename: '[path].br[query]',
      compressionOptions: {level: 11},
      threshold: 10240,
      minRatio: 0.8,
    },
    enabled: true,
    report: false,
    test: /\.(js|css|html|txt|xml|svg)$/,
    // Serving options
    middleware: {
      enabled: true,
      enabledStatic: true,
      encodingsPriority: ['br', 'gzip'],
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    optimization: {
      minimize: true,
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '.',
        name: true,
        minSize: 10000,
        maxSize: 244000
      }
    },
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          esModule: false
        }
      })
    }
  }
}
