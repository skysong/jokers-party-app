export const state = () => ({
  currentNavId: null,
  videoPlay: false,
  audioPlay: false
});

export const mutations = {
  changeCurrentNavId(state, payload) {
    state.currentNavId = payload;
  },
  changeVideoPlay(state, payload) {
    state.videoPlay = payload
  },
  changeAudioPlay(state, payload) {
    state.audioPlay = payload
  }
};
